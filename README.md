# 批量下载（保留目录结构）

#### 项目介绍
通过fiddler抓包获得下载列表，通过下载列表批量下载文件（保留目录结构）

配置

```
//读取下载列表，一行一个url
var downloadFile = "./download.txt";
//保存的时候saveDir会替换url中的downloadHost部分（下载的文件保留目录结构）
var downloadHost = "http://www.baidu.com";
//本地目录
var saveDir = './downloads/'
```

运行 
```
node index.js
```
