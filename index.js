var http = require("http");
var fs = require("fs"); 
var path = require("path");

//读取下载列表，一行一个url
var downloadFile = "./download.txt";
//保存的时候saveDir会替换url中的downloadHost部分（下载的文件保留目录结构）
var downloadHost = "http://www.baidu.com";
//本地保存目录
var saveDir = './downloads/'


var data = fs.readFileSync(path.resolve(__dirname,downloadFile), 'utf8');
var urls = data.split('\n');
var urlTotal = urls.length;
var urlCount = 0;
var unloads=[];
var failLoads=[];

start();
function start(){
    if(urls.length > 0){
        var url = urls.shift();
        if(url && path.extname(url) && url.indexOf("https")==-1){
			if(url.indexOf(downloadHost) == -1){
				console.log("[downloadHost]("+downloadHost+")配置错误，在下载链接中找不到："+url)
				return;
			}
            download(url);
        }else{
            unloads.push(url);
            start();
        }
    }else{
        console.log("download complite");
        console.log("unload:")
        console.log(unloads);
        console.log("failLoad:");
        console.log(failLoads);
    }
}


function download(url){
	url = decodeURI(url);
    urlCount++;
    var req = http.get(url,
    function(res) {
        var imgData = "";
        res.setEncoding("binary");
        res.on("data",
        function(chunk) {
            imgData += chunk;
        });
        res.on("end",
        function() {
            var file = path.resolve(__dirname,url.replace(downloadHost,saveDir));
            file = file.replace("\r","");
            var idx = file.indexOf("?");
            if(idx!=-1){
                file = file.slice(0,idx);
            }
            mkdirsSync(path.dirname(file));
            fs.writeFile(file, imgData, "binary",
            function(err) {
                if (err) {
                    failLoads.push(url);
                    console.warn("保存失败", err);
                    start();
                    return;
                }
                console.log("("+urlCount+"/"+urlTotal+")",url);
                start();
            });
        });
        res.on("error",
        function(err) {
            failLoads.push(url);
            start();
        });
    });
}
//递归创建目录 同步方法
function mkdirsSync(dirname, mode){
    if(fs.existsSync(dirname)){
        return true;
    }else{
        if(mkdirsSync(path.dirname(dirname), mode)){
            fs.mkdirSync(dirname);
            return true;
        }
    }
}
